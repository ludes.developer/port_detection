import requests
from flask import Flask, render_template
app = Flask(__name__)

@app.route('/')
def home():
    port_dict = []
    users = []
    #kloter 1
    for i in range(1,10):
        try:
            resp = requests.get('http://portal-elb-646977374.ap-southeast-1.elb.amazonaws.com:510'+str(i)+'/port_check')
            resp = resp.json()
            if resp == "0":
                port_cond={"port" : "510"+str(i)+" Available",
                            "url" : "http://portal-elb-646977374.ap-southeast-1.elb.amazonaws.com:510"+str(i)
                }
                port_dict.append(port_cond)
                users.append('No User')
            else:
                port_cond={"port" : "510"+str(i)+" Used",
                            "url" : "http://portal-elb-646977374.ap-southeast-1.elb.amazonaws.com:510"+str(i)
                }
                port_dict.append(port_cond)
                users.append(resp)
        except:
            port_cond={"port" : "510"+str(i)+" Unreachable",
                        "url" : "http://portal-elb-646977374.ap-southeast-1.elb.amazonaws.com:510"+str(i)
            }
            port_dict.append(port_cond)
            users.append('No User')
    #kloter 2
    for i in range(10,21):
        try:
            resp = requests.get('http://portal-elb-646977374.ap-southeast-1.elb.amazonaws.com:51'+str(i)+'/port_check')
            resp = resp.json()
            if resp == "0":
                port_cond={"port" : "51"+str(i)+" Available",
                            "url" : "http://portal-elb-646977374.ap-southeast-1.elb.amazonaws.com:51"+str(i)
                }
                port_dict.append(port_cond)
                users.append('No User')
            else:
                port_cond={"port" : "51"+str(i)+" Used",
                            "url" : "http://portal-elb-646977374.ap-southeast-1.elb.amazonaws.com:51"+str(i)
                }
                port_dict.append(port_cond)
                users.append(resp)

        except:
            port_cond={"port" : "51"+str(i)+" Unreachable",
                        "url" : "http://portal-elb-646977374.ap-southeast-1.elb.amazonaws.com:51"+str(i)
            }
            port_dict.append(port_cond)
            users.append('No User')

    return render_template('home.html', ports=port_dict, users=users)

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port='5004')