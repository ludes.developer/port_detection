FROM python:3.8
ADD requirements.txt /
RUN pip install -r requirements.txt
ADD . /usr/local/lib/python3.8/site-packages/port_detection
WORKDIR /usr/local/lib/python3.8/site-packages/port_detection
CMD ["python", "main.py"]
